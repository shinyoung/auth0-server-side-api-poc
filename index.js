const { GraphQLServer } = require('graphql-yoga');
const cookieParser = require('cookie-parser');
const axios = require('axios');

const baseUrl = 'https://dev-rgkg65ar.auth0.com';
const client_id = 'FTG6Ap7WCiKHmyxNYOvvSSmDGhXHDeLd';
const client_secret =
  'hzW2o9e_A0QPCM0GT0-_NYajtEt5k8Y945PXZeMqKL4giTDpgJnF0tXoVmZgy7ha';
const connection = 'Username-Password-Authentication';

const signUp = async (email, password) => {
  try {
    const res = await axios.post(baseUrl + '/dbconnections/signup', {
      connection,
      client_id,
      client_secret,
      email,
      password
    });
    console.log(res.data);
  } catch (error) {
    console.error(error);
  }
};

const logIn = async (username, password, phone_number) => {
  try {
    const res = await axios.post(baseUrl + '/oauth/token', {
      grant_type: 'password',
      connection,
      client_id,
      client_secret,
      username,
      password
    });
    console.log(res.data);
  } catch (error) {
    if (error.response.data.error === 'mfa_required') {
      const mfaToken = error.response.data.mfa_token;
      const oobCode = await requestMFAAssociation(phone_number, mfaToken);
      return { mfaToken, oobCode };
    }
  }
};

const requestMFAAssociation = async (phone_number, mfa_token) => {
  try {
    const res = await axios.post(
      baseUrl + '/mfa/associate',
      {
        client_id,
        client_secret,
        authenticator_types: ['oob'],
        oob_channels: ['sms'],
        phone_number: '+1 ' + phone_number
      },
      {
        headers: {
          authorization: 'Bearer ' + mfa_token
        }
      }
    );
    return res.data.oob_code;
  } catch (error) {
    console.error(error);
  }
};

const verifyMFA = async (binding_code, mfa_token, oob_code) => {
  try {
    const res = await axios.post(baseUrl + '/oauth/token', {
      client_id,
      client_secret,
      mfa_token,
      oob_code,
      binding_code,
      grant_type: 'http://auth0.com/oauth/grant-type/mfa-oob'
    });
    console.log(res.data);
  } catch (error) {
    console.error(error);
  }
};

const context = ({ request, response }) => ({
  request,
  response
});

const typeDefs = `
  type Query {
    logIn(input: LogInInput!): LogInOutput
  }

  type Mutation {
    signUp(input: SignUpInput!): Boolean
    verifyMFA(input: VerifyMFAInput!): Boolean
  }

  input SignUpInput {
    email: String!
    password: String!
  }

  input LogInInput {
    email: String!
    password: String!
    phone: String!
  }

  type LogInOutput {
    oobCode: String
    mfaToken: String
  }

  input VerifyMFAInput {
    bindingCode: String!
    mfaToken: String!
    oobCode: String!
  }
`;

const resolvers = {
  Query: {
    logIn(_, { input }, { response }, info) {
      const { email, password, phone } = input;
      response.addCookie();
      return logIn(email, password, phone);
    }
  },
  Mutation: {
    signUp(_, { input }) {
      const { email, password } = input;
      return signUp(email, password);
    },
    verifyMFA(_, { input }) {
      const { bindingCode, mfaToken, oobCode } = input;
      return verifyMFA(bindingCode, mfaToken, oobCode);
    }
  }
};

const server = new GraphQLServer({ typeDefs, resolvers, context });
server.express.use([cookieParser()]);
server.start(() => console.log(`Server is running at http://localhost:4000`));
